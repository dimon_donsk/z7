<?php
class Products
{
    public $name;
    public $cost;
    public $weight;
    public function __construct($name = 'Продукт',$cost = 'Бесценно',$weight = 'Невесомо')
    {
        $this->name = $name;
        $this->cost = $cost;
        $this->weight = $weight;
    }
    public function printProduct()
    {
        echo "Наименование: $this->name <br>Стоимость: $this->cost <br>Вес: $this->weight <br><br>";
    }

}

$pelmeshki = new Products('Пельмешки');
$hleb = new Products('Хлеб', 25, 0.5);
$moloko = new Products('Молоко', 56, 1);
$pelmeshki-> printProduct();
$hleb->printProduct();
$moloko->printProduct();